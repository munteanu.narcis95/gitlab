package automation_week3;


import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseTestClass;

/*Access the web page https://www.webdriveruniversity.com/Popup-Alerts/index.html
        Identify the second button and click on it
        Verify that the modal is displayed, ensuring that the close button is present on the page
        Ensure that the modal’s close button is NOT visible immediately after its opening
        Wait for a few seconds .
        Verify that the close button of the modal becomes visible after the waiting interval.
        Click the close button of the modal.
        Wait for a few seconds
        Verify that the modal is no longer displayed on the page
*/
public class ExercitiiClasa extends BaseTestClass {

    @Test

    public void alertMethod() throws InterruptedException {

    String url = "https://www.webdriveruniversity.com/Popup-Alerts/index.html";
    driver.get(url);

        WebElement ClickMeButton = driver.findElement(By.id("button2"));
        ClickMeButton.click();

        WebElement CloseButton = driver.findElement(By.xpath("//button[@class='btn btn-default']"));

        Assert.assertTrue("The Close Button is displayed", !CloseButton.isDisplayed());    // ! = negatie

        Thread.sleep(3000);

        Assert.assertTrue("Butonul CLOSE nu este afisat", CloseButton.isDisplayed());
        CloseButton.click();

        Thread.sleep(3000);

        Assert.assertTrue("The Close Button is displayed", !CloseButton.isDisplayed());


    }

}
