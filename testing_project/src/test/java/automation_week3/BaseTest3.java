package automation_week3;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest3 {

    public WebDriver driver;

    @Before

    public void setUp() {
        System.out.println("Create driver: Chrome");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    /*@After

    public void tearDown() {
        System.out.println("Close Driver");
        driver.quit();
    }

     */
}
