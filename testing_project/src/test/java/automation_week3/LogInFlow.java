package automation_week3;

/*Navigate to the web page https://testare-manuala.locdejoacapentruitsti.com/blog/login/
Select the username input field
Clear the existing text in the field
Enter a new username
Clear the text in the field
Enter the same username in uppercase letters using the SHIFT key.
Copy the text from the username field
Select the password input field
Paste the copied text into the password field
 */

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.BaseTestClass;

public class LogInFlow extends BaseTestClass {

    @Test

    public void ExerciseFour() throws InterruptedException {

        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");
        WebElement usernameField = driver.findElement(By.id("user_login"));
        WebElement passwordField = driver.findElement(By.id("user_pass"));

        Actions butoane = new Actions(driver);
        butoane.moveToElement(usernameField)
                .click()
                .keyDown(Keys.CONTROL)
                .sendKeys("a")
                .keyUp(Keys.CONTROL)
                .sendKeys(Keys.DELETE)
                .build()
                .perform();

        Thread.sleep(3000);


        butoane.moveToElement(usernameField)
                .click()
                .sendKeys("username")
                .build()
                .perform();

        Thread.sleep(3000);

        butoane.moveToElement(usernameField)
                .click()
                .keyDown(Keys.CONTROL)
                .sendKeys("a")
                .keyUp(Keys.CONTROL)
                .sendKeys(Keys.DELETE)
                .build()
                .perform();

        Thread.sleep(3000);

        butoane.moveToElement(usernameField)
                .click()
                .keyDown(Keys.SHIFT)
                .sendKeys("username")
                .keyUp(Keys.SHIFT)
                .build()
                .perform();

        butoane.moveToElement(usernameField)
                .click()
                .keyDown(Keys.CONTROL)
                .sendKeys("a")
                .sendKeys("c")
                .keyUp(Keys.CONTROL)
                .build()
                .perform();

        Thread.sleep(3000);

        butoane.moveToElement(passwordField)
                .click()
                .keyDown(Keys.CONTROL)
                .sendKeys("v")
                .keyUp(Keys.CONTROL)
                .build()
                .perform();



    }


}
