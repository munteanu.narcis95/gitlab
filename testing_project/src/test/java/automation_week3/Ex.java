package automation_week3;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import utils.BaseTestClass;

public class Ex  extends BaseTestClass {

    @Test

    public void Exercitiu () {

        driver.get("https://output.jsbin.com/osebed/2");

        WebElement element1 = driver.findElement(By.xpath("//select/option[@value='banana']"));
        WebElement element2 = driver.findElement(By.xpath("//select/option[@value='apple']"));

        Actions selectareMultipla = new Actions(driver);

        Action actiuniPeElemente = selectareMultipla.keyDown(Keys.CONTROL)
                .click(element1)
                .click(element2)
                .keyUp(Keys.CONTROL)
                .build();

        actiuniPeElemente.perform();


        Assert.assertTrue("Elementul 1 nu este selectat", element1.isSelected());
        Assert.assertTrue("Elementul 2 nu este selectat", element2.isSelected());

    }


}
