package automation_week3;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseTestClass;


        /*Navigate to the web page https://www.webdriveruniversity.com/Popup-Alerts/index.html
        Identify and click on the fourth the button
        Wait for a few seconds
        Dismiss the alert
        Assert that the text of the confirmation message matches the expected text: “You pressed Cancel!“.
        */

public class DismissAllert extends BaseTestClass {

    @Test

    public void exercitiu2 () throws InterruptedException {

        driver.get("https://www.webdriveruniversity.com/Popup-Alerts/index.html");

        WebElement clickMeButton = driver.findElement(By.id("button4"));
        clickMeButton.click();

        Thread.sleep(3000);

        driver.switchTo().alert().dismiss();

        WebElement dismissAllert = driver.findElement(By.id("confirm-alert-text"));
        String expectedMessage = "You pressed Cancel!";

        Assert.assertEquals(expectedMessage, dismissAllert.getText());
    }
}
