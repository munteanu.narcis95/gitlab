package automation_week3;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import utils.BaseTestClass;

import java.util.List;

        /*1. deschide site-ul https://www.mediacollege.com/internet/samples/html/country-list.html
        2. verifica optiunea implicita / selectata in drop-down
        3. Verificam ca avem optiunea ‘Romania’ disponibila
        4. Verificam ca ultima optiune din lista este ’Zimbabwe;
        */

public class SelectExercise extends BaseTestClass {

    @Test

    public void exercitiu3() {

        String url = "https://www.mediacollege.com/internet/samples/html/country-list.html";
        driver.get(url);

        WebElement implicitOption = driver.findElement(By.name("country"));

        Select primaOptiune = new Select(implicitOption);

        String primaOptiuneText = primaOptiune.getFirstSelectedOption().getText();

        System.out.println(primaOptiuneText);

        Assert.assertEquals("Optiunea nu este cea dorita", "Country...", primaOptiuneText);

        List<WebElement> ListaTari = primaOptiune.getOptions();

        boolean isThereRomania = false;

        for (WebElement optiuneCurenta : ListaTari) {


            if (optiuneCurenta.getText().equals("Romania")) {
                isThereRomania = true;
                System.out.println("Pentru ca am gasit Romania in lista, ma opresc din a cauta");
                break;
            } else {
                System.out.println("Continui cautarea");
            }
            System.out.println("Optiunea Romania apare in lista:" + isThereRomania);
        }


            int lista = ListaTari.size();
            int pozitieUltimulElement = lista - 1;

            String pozitieUltimulElementText = ListaTari.get(pozitieUltimulElement).getText();

            Assert.assertEquals("Ultima tara nu este Zimbabwe", "Zimbabwe", pozitieUltimulElementText);

        System.out.println(pozitieUltimulElementText);
        }
    }

