package Automation1;

import org.junit.Test;
import utils.BaseTestClass;

public class MyFirstTest extends BaseTestClass {


    @Test

    public void PageSource() {

        //deschidere browser + navigare
        //obiect.metoda


        //varianta 1 de a navita pe un site cu .get()


        driver.get("https://wantsome.ro/");

        //vairanta 2 de a naviga cu navigate.to

        driver.navigate().to("https://wantsome.ro/");


        String sourceCod = driver.getPageSource();

        System.out.println(sourceCod);


    }
}
