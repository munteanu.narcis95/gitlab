package Automation1;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PositiveTests {

    public WebDriver driver;

    @Before
    public void setup() {
        System.out.println("Create driver: chrome");

        driver = new ChromeDriver();
        driver.manage().window().maximize();

    }

    @Test
    public void loginTest() {
        System.out.println("Starting login test");

        //open main page

        String url = "https://the-internet.herokuapp.com/";

        driver.get(url);

        //Click on the Form Authentication link

        driver.findElement(By.linkText("Form Authentication")).click();

        //enter username and password

        driver.findElement(By.id("username")).sendKeys("tomsmith");
        driver.findElement(By.id("password")).sendKeys("SuperSecretPassword!");

        // push login button

        driver.findElement(By.className("radius")).click();

        

    }
}
