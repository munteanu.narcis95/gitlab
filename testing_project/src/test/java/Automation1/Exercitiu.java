package Automation1;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseTestClass;

public class Exercitiu extends BaseTestClass {

    @Test

    public void AccesareSite() {

        driver.get("https://www.emag.ro/");

        driver.navigate().to("https://www.emag.ro/mouse/c?ref=hp_menu_quick-nav_23_22&type=category");

        driver.navigate().to("https://www.emag.ro/mouse-gaming-logitech-g102-lightsync-8000-dpi-rgb-negru-910-005823/pd/D6P2GMMBM/");

        String mouseCorect = driver.getTitle();

        String expectedMouse = "Mouse gaming Logitech G102 Lightsync, 8000 dpi, RGB, Negru - eMAG.ro";

        Assert.assertEquals(expectedMouse, mouseCorect);

        WebElement searchBox =
                driver.findElement(By.id("searchboxTrigger"));
        searchBox.click();

        searchBox.sendKeys("Tastatura");

        searchBox.submit();


        WebElement tastaturaAleasa = driver.findElement(By.xpath("//*[@href='https://www.emag.ro/kit-gaming-a-hl1-4-in-1-tastatura-mouse-casti-mousepad-kgshl1/pd/DD18K6BBM/']"));

        tastaturaAleasa.click();

        WebElement adaugaInCos = driver.findElement(By.xpath("//*[@class='btn btn-xl btn-primary btn-emag btn-block main-button gtm_680klw yeahIWantThisProduct']"));

        adaugaInCos.click();
    }


}
