package Automation1;

import org.junit.Test;
import utils.BaseTestClass;

public class Navigation extends BaseTestClass {


    @Test

    public void BrowserNav(){

        driver.get("https://www.emag.ro");

        driver.navigate().to("https://www.emag.ro/desktop-pc/c?ref=hp_menu_quick-nav_23_1&type=category");

        driver.navigate().refresh();

        driver.navigate().back();

        driver.navigate().forward();
    }



}
