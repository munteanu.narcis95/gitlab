package Automation1;

import org.junit.Assert;
import org.junit.Test;
import utils.BaseTestClass;

public class WantsomeExercise extends BaseTestClass {


    @Test


    public void CourseVerification() {

        driver.get("https://www.wantsome.ro/");

        //sa compar titlul asteptat cu rezultatul actual

        String currentPageTitle = driver.getTitle(); //actual result

        String expectedTitle = "Wantsome - Academia prietenoasă de IT - Cursuri IT România";

        Assert.assertEquals(expectedTitle, currentPageTitle);

        driver.navigate().to("https://wantsome.ro/cursuri/curs-de-introducere-in-programare/");

        String currentCourseTitle = driver.getTitle();

        String expectedCourseTitle = "Curs de Introducere în Programare - Wantsome";

        Assert.assertEquals(expectedCourseTitle, currentCourseTitle);

        driver.navigate().back();

        //varianta 1

        currentPageTitle = driver.getTitle();

        Assert.assertEquals(expectedTitle, currentPageTitle);

        driver.navigate().forward();

        currentCourseTitle = driver.getTitle();

        Assert.assertEquals(expectedCourseTitle, currentCourseTitle);


        driver.navigate().refresh();

        Assert.assertEquals(expectedCourseTitle, driver.getTitle());




    }
}
