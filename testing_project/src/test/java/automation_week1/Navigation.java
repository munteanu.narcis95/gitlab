package automation_week1;

import org.junit.Test;
import utils.BaseTestClass;

public class Navigation extends BaseTestClass {

    @Test

    public void BrowserNav() {

        driver.get("https://www.emag.ro");

        driver.navigate().to("https://www.emag.ro/jucarii-copii-bebe/d?ref=hdr_menu_department_12");

        driver.navigate().refresh();

        driver.navigate().back();

        driver.navigate().forward();


    }


}
