package automation_week1;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.BaseTestClass;

public class MyFirstTest extends BaseTestClass {

@Test

    public void PageSource() {

    //deschidere browser + navigare
    //obiect.metoda

    //varianta 1 de a naviga pe un site cu .get()

    driver.get("https://wantsome.ro");

    //varianta 2 de a naviga cu navigate.to

    driver.navigate().to("https://wantsome.ro");


    String sourceCode = driver.getPageSource();


    System.out.println(sourceCode);



}



}


