package automation_week1;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseTestClass;

import java.util.List;

public class LocatorTypes extends BaseTestClass {

@Test

    public void findElementById () {

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

    //identificam elementul/elementele cu care lucram

    WebElement checkBox = driver.findElement(By.id("rememberme"));


    //Assert = verificare

    //Assert true = raspunde la intrebarea "este conditia adevarata?"

    //Assert false = raspunde la intrebarea "este conditia falsa?"

    //Assert equals = compara un expected result pe care il definim noi si actual result (adica ceea ce gaseste in pagina)


    Assert.assertFalse("Verify if rememberme checkbox is not selected", checkBox.isSelected());
    // adevarat => conditia 'checkbox is selected' este falsa
    // daca ar fi selected, mesajul ar veni fals = checkbox is not selected

   //click pe checkbox - actiune (manipulare)

    checkBox.click();


    Assert.assertTrue("Verify that checkbox is selected", checkBox.isSelected());


}

@Test

    public void findElementByClassName() {

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

    WebElement searchBar = driver.findElement(By.className("search-field"));

    String actualPlaceHolder = searchBar.getAttribute("placeholder"); //actual result

    Assert.assertEquals("Search …", actualPlaceHolder);



}
@Test

    public void findElementByName() {

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

    WebElement usernameField = driver.findElement(By.name("log"));

    WebElement passwordField = driver.findElement(By.name("pwd"));

    //o verificare pentru fiecare field
    Assert.assertEquals("text", usernameField.getAttribute("type"));

    Assert.assertEquals("password", passwordField.getAttribute("type"));

}

@Test

    public void findElementByLinkText() {

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

    WebElement textLink = driver.findElement(By.linkText("February 2020")); //aduce link-ul din spatele elementului cu textul February 2020


    Assert.assertEquals("https://testare-manuala.locdejoacapentruitsti.com/blog/2020/02/", textLink.getAttribute("href"));


}

@Test

    public void findElementByPartialLinkTest() {

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

    //are rolul de a identifica primul element care contine...

    WebElement berElement = driver.findElement(By.partialLinkText("ber 2018"));

    Assert.assertEquals("December 2018", berElement.getText());

    List <WebElement> links = driver.findElements(By.partialLinkText("ber 2018"));

    //am un numar de 3 elemente - sa fac o verificare ca in pagina actual result = 3 elemente

    int expectedNumberOfResults = 3;

    Assert.assertEquals(expectedNumberOfResults, links.size());


    // sa verificam ca textul fiecarui element din lista este pe rand, December 2018, November 2018, October 2018
    //textul primului element din lista

    Assert.assertEquals("December 2018", links.get(0).getText());
    Assert.assertEquals("November 2018", links.get(1).getText());
    Assert.assertEquals("October 2018", links.get(2).getText());


}
@Test

    public final void findElementByXpathWithName () {

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

    WebElement textField = driver.findElement(By.xpath("//*[@name='log']"));

    Assert.assertEquals("", textField.getAttribute("value"));


}

@Test

    public final void findElementByXpathWithClassName(){

    driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");


    WebElement searchbox = driver.findElement(By.xpath("//*[@class='search-field']"));

    String actualSearchPlaceHolder = searchbox.getAttribute("placeholder"); //actual result

    Assert.assertEquals("Search …",  actualSearchPlaceHolder);
}


}
