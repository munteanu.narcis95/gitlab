package automation_week1;

import org.junit.Assert;
import org.junit.Test;
import utils.BaseTestClass;

public class WantsomeExercise extends BaseTestClass {


    /**
     * ● Build a test that navigates to https://www.wantsome.ro/
     * ● Then go to the course page https://wantsome.ro/cursuri/curs-de-introducere-in-programare/
     * while maintaining the browser history and cookies.
     * ● Go back.
     * ● Go forward.
     * ● Refresh the page.
     * ● After each step, find verify that you are on the correct page by
     * checking the title.
     */

    @Test

    public void CourseVerification() {

        driver.get("https://wantsome.ro");

        //sa compar titlul asteptat cu rezultatul actual

        String currentTitle = driver.getTitle(); //actual result

        String expectedTitle = "Wantsome - Academia prietenoasă de IT - Cursuri IT România";


        //varianta cu if

      /*  if (currentTitle.equals(expectedTitle)) {
            System.out.println("Titlul este cel asteptat");

        } else {
            System.out.println("Titlul este gresit");

       */


            //varianta cu assert

            Assert.assertEquals(expectedTitle, currentTitle);

       driver.navigate().to("https://wantsome.ro/cursuri/curs-de-introducere-in-programare/");

       String currentCourseTitle = driver.getTitle(); //actual

       String expectedCourseTitle = "Curs de Introducere în Programare - Wantsome"; //expected


       Assert.assertEquals(expectedCourseTitle, currentCourseTitle);

       //Actiunea Back
       driver.navigate().back();

       //varianta 1

       //currentTitle = driver.getTitle();

       //Assert.assertEquals(expectedTitle, currentTitle);


       //varianta 2

        Assert.assertEquals(expectedTitle, driver.getTitle());


       //Actiunea de Forward
       driver.navigate().forward();


       currentCourseTitle = driver.getTitle();

       Assert.assertEquals(expectedCourseTitle, currentCourseTitle);

       //Actiunea de refresh
        driver.navigate().refresh();

        Assert.assertEquals(expectedCourseTitle, driver.getTitle());






        }
    }
