package automation_week2;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;

public class PositiveTests extends BaseTest {


    @Test
    public void loginTest() {
        System.out.println("Starting login test");

        //open main page
        String url = "https://the-internet.herokuapp.com/";
        driver.get(url);

        //Click on the Form Authentication link
        WebElement formAuth = driver.findElement(By.linkText("Form Authentication"));
        formAuth.click();

        //enter username and password
        driver.findElement((By.id("username"))).sendKeys("tomsmith");
        driver.findElement((By.id("password"))).sendKeys("SuperSecretPassword!");

        //push login button
        driver.findElement(By.className("radius")).click();

        //validate url is correct
        String expectedUrl = "https://the-internet.herokuapp.com/secure";
        assertEquals(expectedUrl, driver.getCurrentUrl());

        //validate logout button is displayed
        WebElement logOutButton = driver.findElement(By.xpath("//a[@class='button secondary radius']"));
        Assert.assertTrue("LogOutButton is not visible", logOutButton.isDisplayed());

        //Successfull log in message
        String expectedSuccessMessage = "You logged into a secure area!\n×";

        String actualSuccessMessage = driver.findElement(By.id("flash")).getText();

        //Assert.assertTrue("actualSuccessMessage does not contain expectedSuccessMessage \n expectedSuccessMessage: "
        //                +expectedSuccessMessage +"\nactualSuccessMessage: " +actualSuccessMessage, actualSuccessMessage.contains(expectedSuccessMessage));

        Assert.assertEquals("Expected success message is not correct", expectedSuccessMessage, actualSuccessMessage);

    }

}
