package automation_week2;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DropDowns extends BaseTest{

    @Test
    public void DropDowns() throws InterruptedException {
        //Go to home page
        String url = "https://the-internet.herokuapp.com/";
        driver.get(url);

        driver.findElement(By.linkText("Dropdown")).click();

        //Find dropdown element
        WebElement dropDownElement = driver.findElement(By.id("dropdown"));


        //Create a select object
        Select dropdown = new Select(dropDownElement);

        //Select option 2 by its value attribute
        dropdown.selectByValue("2");

        //Verify Option 2 is selected
        String selectedOption = dropdown.getFirstSelectedOption().getText();
        Assert.assertEquals("Option was not correctly set", "Option 2", selectedOption);

        //Select option 1 by its text
        dropdown.selectByVisibleText("Option 1");
        selectedOption = dropdown.getFirstSelectedOption().getText();
        Assert.assertEquals("Option 1", selectedOption);

        Thread.sleep(5000);
    }
}
