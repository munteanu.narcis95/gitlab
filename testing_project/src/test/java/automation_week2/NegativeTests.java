package automation_week2;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class NegativeTests extends BaseTest {


    @Test
    public void negativeTest() {
        System.out.println("Starting login test");

        //open main page
        String url = "https://the-internet.herokuapp.com/";
        driver.get(url);

        //Click on the Form Authentication link
        WebElement formAuth = driver.findElement(By.linkText("Form Authentication"));
        formAuth.click();

        //enter username and password
        driver.findElement((By.id("username"))).sendKeys("admin");
        driver.findElement((By.id("password"))).sendKeys("invalidPassword");

        //push login button
        driver.findElement(By.className("radius")).click();

        String expectedErrorMessage = "Your username is invalid!\n×";
        String actualErrorMessage = driver.findElement(By.id("flash")).getText();

        Assert.assertEquals("Invalid error message", expectedErrorMessage, actualErrorMessage);
    }

}
