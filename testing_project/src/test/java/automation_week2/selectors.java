package automation_week2;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class selectors extends BaseTest {


    //Xpath
    //Syntax: //Tagname[@Attr1='value']

    //Css selectors using attributes
    /*Syntax: TagName[Attr1='value']
    *
    * In case we use 'id' or 'class' as an attribute we can use
    *
    *           id => #
    *           class => .
    * Example: div#u123         span.layerparent
    * */

    @Test
    public void cssSelectors() {
        //open main page
        String url = "https://the-internet.herokuapp.com/";
        driver.get(url);

        //Click on the Form Authentication link
        WebElement formAuth = driver.findElement(By.linkText("Forgot Password"));
        formAuth.click();
        driver.findElement(By.cssSelector("input[id='email']"));
        driver.findElement(By.cssSelector("input#email")).sendKeys("testam");

        //Found the same element using different format
        driver.findElement(By.cssSelector("i[class='icon-2x icon-signin']"));
        driver.findElement(By.cssSelector("i.icon-2x"));
    }
}
