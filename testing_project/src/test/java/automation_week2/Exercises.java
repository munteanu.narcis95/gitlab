package automation_week2;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class Exercises extends BaseTest {

        @Test
        public void DropDowns()  {

            String url = "https://www.mediacollege.com/internet/samples/html/country-list.html";
            driver.get(url);

            WebElement dropDownElement = driver.findElement(By.cssSelector("select[name='country']"));

            Select dropdown = new Select(dropDownElement);

            WebElement firstOption = dropdown.getFirstSelectedOption();

            String firstOptionText = firstOption.getText();

            Assert.assertEquals("Country...", firstOptionText);


    }



}


