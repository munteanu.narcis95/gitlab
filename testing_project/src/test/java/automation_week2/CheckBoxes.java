package automation_week2;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class CheckBoxes extends BaseTest {

    @Test
    public void CheckBoxes() {

        //open main page
        String url = "https://the-internet.herokuapp.com/";
        driver.get(url);

        driver.findElement(By.linkText("Checkboxes")).click();

        //select all checkboxes
        List<WebElement> checkboxes = driver.findElements(By.xpath("//form[@id='checkboxes']//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes) {
            if(!checkbox.isSelected()) {
                checkbox.click();
            }
        }

        //Assert that all checkboxes are checked
        for(WebElement checkBox : checkboxes) {
            assertTrue("Checkbox is not checked", checkBox.isSelected());
        }

        checkboxes.clear();
        checkboxes.size();
    }
}
