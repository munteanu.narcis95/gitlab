package automation_week4;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.BaseTestClass;

import java.time.Duration;

public class TrashExercise extends BaseTestClass {

    @Test

    public void Trash1 () {

        driver.get("https://www.globalsqa.com/demo-site/draganddrop/");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        WebElement BoxOfElements = driver.findElement(By.xpath("//iframe[@class='demo-frame lazyloaded']"));

        driver.switchTo().frame(BoxOfElements);

        WebElement ElementToDrag = driver.findElement(By.xpath("//li[@class='ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle']"));


        WebElement Trash = driver.findElement(By.id("trash"));

        Actions action = new Actions(driver);
        action.dragAndDrop(ElementToDrag, Trash).build().perform();

        WebElement elementInTrash = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='trash']//li[@class='ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle']")));

    }

}
