package automation_week4;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.BaseTestClass;

import java.time.Duration;

public class ExplicitEx extends BaseTestClass {

    @Test

    public void ExercitiuExplicitWait () {

        driver.get("https://chandanachaitanya.github.io/selenium-practice-site/?languages=Java&enterText=");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        WebElement alertButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("alertBox")));

        Assert.assertTrue(alertButton.isDisplayed());




    }



}
