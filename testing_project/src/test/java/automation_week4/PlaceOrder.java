package automation_week4;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import utils.BaseTestClass;



        /*add 1 kilo of carrots, 1 kilo of cucumbers and 2 kilos of potatoes to the cart
        proceed with the checkout and place the order
        proceed by choosing as a country and ticking Terms and Condition button
        check that can see the successful text that order has been placed
        */
public class PlaceOrder extends BaseTestClass {

    @Test

            public void adaugareCos () throws InterruptedException {

            driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");

        WebElement carrotButton = driver.findElement(By.xpath("(//button[contains(text(), 'ADD TO CART')])[5]"));
        carrotButton.click();

        WebElement cucumbersButton = driver.findElement(By.xpath("(//button[contains(text(), 'ADD TO CART')])[3]"));
        cucumbersButton.click();

        WebElement potatoesPlus = driver.findElement(By.xpath("(//a[contains(text(), '+')])[11]"));
        potatoesPlus.click();

        WebElement potatoesButton = driver.findElement(By.xpath("(//button[contains(text(), 'ADD TO CART')])[9]"));
        potatoesButton.click();

        WebElement cartButton = driver.findElement(By.xpath("//img[@alt='Cart']"));
        cartButton.click();

        WebElement proceedButton = driver.findElement(By.xpath("(//button[contains(text(), 'PROCEED TO CHECKOUT')])"));
        proceedButton.click();

        Thread.sleep(5000);

        WebElement placeOrderButton = driver.findElement(By.xpath("(//button[contains(text(), 'Place Order')])"));
        placeOrderButton.click();

        WebElement country = driver.findElement(By.xpath("//option[@value='Afghanistan']"));
        country.click();

        Select buttonList = new Select(driver.findElement(By.xpath("//select")));
        buttonList.getFirstSelectedOption();
    }




}
