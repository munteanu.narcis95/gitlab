#Web Element Interrogation Exercises
Go to https://testare-manuala.locdejoacapentruitsti.com/blog/contact/

1. Find the "Software Testing" button from the menu and identify it through its id. Assert that its text value is correct.

2. Find the ID for the Facebook button (upper right side). Extract its text. What would you expect the value to be?

3. Find the Email input and assert on its tag name and that it's displayed.

4. Find the WordPress link and assert that it is underlined. Check the css value in dev tools.

5. Find all inputs - print their number on the screen.

6. Find all the available selections from the dropdown - assert on their size, get the first ones text and print it on the screen, find the longest one and print it on the screen.

7. Find all the social media links chaining find by's and assert on size

8. Go back to exercise 2. Can you prove that the text visible in the DOM element is not visible?