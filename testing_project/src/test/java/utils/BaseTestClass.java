package utils;
import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
public class BaseTestClass {
    public WebDriver driver;

    @Before
    public void setUpTest() {
        // for selenium dependency 4.5 and lower, update the drivers with current version

        driver = new ChromeDriver();
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }


/*
    @Before
    public void setUpTest() {
        System.setProperty("webdriver.gecko.driver",
                "/Users/danielpaval/IdeaProjects/all_combined13/curs/in_clasa/src/test/resources/drivers/mac/geckodriver");
        driver = new FirefoxDriver();
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }

 */

   // @After // Executed After each test method
  //public void tearDown() {
      //driver.quit();
   //}
}