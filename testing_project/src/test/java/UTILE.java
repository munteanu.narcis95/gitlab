public class UTILE {


/*  ACTION CLASS ->

    Actions builder = new Actions(driver);
    Action mouseOver = builder.moveToElement(element).build();
    mouseOver.perform()

   ● keyDown(modifier_key) - Keys.ALT, Keys.SHIFT, Keys.CONTROL, etc
   ● keyUp(modifier_key) - Keys.ALT, Keys.SHIFT, Keys.CONTROL, etc
   ● sendKeys(onElement, charsequence) - Sends a series of keystrokes onto the element

    ○ .click - self explanatory
    ○ .doubleClick - performs a double-click at the current mouse location
    ○ .release - release the mouse button
    ○ .clickAndHold - clicks (without releasing) at the current mouse location
    ○ .contextClick - performs a right click at the current mouse location
    ○ .moveToElement(element) - moves mouse to the middle of the element

    Actions action = new Actions(driver);
    action.moveToElement(element).sendKeys(Keys.ARROW_DOWN)
            .sendKeys(Keys.ESCAPE)
            .sendKeys(Keys.CTRL)
            .sendKeys(Keys.ENTER);
    action.build().perform();

    Actions action = new Actions(driver);
        action.moveToElement(webElement).clickAndHold().release()
                .doubleClick(webElement).dragAndDrop(source, destination)
                .contextClick(webElement);
    action.build().perform();

    ALERTS ->

    ● Cancel the alert
        ○ driver.switchTo().alert().dismiss();
    ● Accept the alert
        ○ driver.switchTo().alert().accept();
    ● Get alert message
        ○ driver.switchTo().alert().getText();
    ● Send keys to an alert (some alers are asking for credentials, for example)
        ○ driver.switchTo().alert().sendKeys("Text");


    SYNCHRONIZATION -> Implicit Wait & Explicit Wait ->

    Implicit wait ->
            ○ driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    Explicit wait ->
              WebDriverWait wait = new WebDriverWait(driver, 10);
              WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id")));


    SELECT - DROPDOWN ->

            Select dropdown = new Select(WebElement)
                dropdown.getAllSelectedOptions()
                        .selectByValue(String value)
                        .selectByIndex(int index)
                        .getFirstSelectedOption()
                        .selectByVisibleText(String)
                        .deselectAll()
                        .isMultiple()
                        .getOptions()

     XPATH & CSSelector

     CSS Selector -> WebElement firstName = driver.findElement(By.cssSelector("input[name='first_name']"));
     XPath -> //tagname[@attribute='value']




 */







}
