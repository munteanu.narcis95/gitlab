package ExercitiiSingur;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseTestClass;

public class ProdusInCos extends BaseTestClass {

    @Test

    public void ProdusCos() throws InterruptedException {

        String url = "https://www.emag.ro/";
        driver.get(url);

        Thread.sleep(3000);

        WebElement allertButton = driver.findElement(By.xpath("//button[@class='btn btn-primary js-accept gtm_h76e8zjgoo btn-block']"));
        allertButton.click();

        WebElement tableteTelefoane = driver.findElement(By.xpath("//a[@href='/laptop-tablete-telefoane/d?ref=hdr_menu_department_1']"));
        tableteTelefoane.click();

        driver.navigate().back();

        WebElement searchBox = driver.findElement(By.id("searchboxTrigger"));
        searchBox.sendKeys("Tableta");
        searchBox.submit();

        WebElement produsDeAdaugat = driver.findElement(By.xpath("//a[@href='https://www.emag.ro/tableta-10-inci-android-11-4gb-ram-64gb-rom-camera-dubla-baterie-de-6000mah-negru-735812472829/pd/D4N68WMBM/']"));
        produsDeAdaugat.click();

        WebElement adaugaInCosButton = driver.findElement(By.xpath("//button[@class='btn btn-xl btn-primary btn-emag btn-block main-button gtm_680klw yeahIWantThisProduct']"));
        adaugaInCosButton.click();

        Thread.sleep(2000);

        WebElement detaliiCos = driver.findElement(By.xpath("//a[@href='/cart/products?ref=add-to-cart-module_go-to-cart_button']"));
        detaliiCos.click();




    }



}
