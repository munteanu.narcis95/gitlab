package java_week1;

public class Variables {


    public static void main (String [] args) {

        //varianta 1 de lucru cu variabile - declar si assignez o valoare variabilei de la bun inceput

        String numeleMeu = "Stefana";


        //varianta 2 de lucru cu variabile  - declar variabila si sa ii assignez valoarea pe parcursul codului


        String celalaltNume;

        System.out.println("Hello world");
        System.out.println("Hello world");
        System.out.println("Hello world");
        System.out.println("Hello world");

        //apelez variabila si ii dau o valoare

        celalaltNume = "Vasilache";


        int ziuaDeNastere = 10;
        final int lunaNasterii = 3;

        //variabila numarulMeuNorocos este obtinuta printr-un calcul definit
        int numarulMeuNorocos = ziuaDeNastere - lunaNasterii;

        //varianta 2 de a afisa "Hello World"

        String primulCuvant = "Hello";
        String alDoileaCuvant = "World";

        String propozitiaMea = primulCuvant + alDoileaCuvant;


        //apelez valoarea stocata in variabila propoziaMea
        System.out.println(propozitiaMea);


        //varianta 3 de a afisa "Hello World"


        String cuvantulMeu = "Hello";
        int numar = 10;
        int numarTest = 4;
//Hello World
        System.out.println(numar + numarTest + cuvantulMeu + "World" + numar + numarTest);


//numerele se aduna pana cand ajungem la adunarea unei date de tip string, de aici inainte se face doar alipire pentru indiferent cat de multe numere sunt

//reassignare a valorii
        ziuaDeNastere = 27;



    }
}
