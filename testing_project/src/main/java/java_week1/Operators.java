package java_week1;

public class Operators {

    public static void main(String[] args) {
//salariu peste 3000 de lei si vechime peste 2 ani SAU salariu peste 5000 de lei si vechime minim 1an, atunci afisam un mesaj ca putem face un credit


        int salariu = 4000;
        int vechime = 3;

        int suma;
        if (salariu > 3000 && vechime >=2 || salariu > 5000 && vechime >= 1) {
            suma = 5000;
            System.out.println("Pot face credit");
            System.out.println("Vei primii suma: " + suma);
        } else {
            suma =0;
            System.out.println("Nu pot face credit");
            System.out.println("Vei primii suma: " + suma);
        }

        int mesajBanca = (salariu > 3000 && vechime >=2 || salariu > 5000 && vechime >= 1) ? 5000 : 0;

        System.out.println("Da chiar primesti: " + mesajBanca);

    }
}