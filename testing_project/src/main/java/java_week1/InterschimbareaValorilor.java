package java_week1;

public class InterschimbareaValorilor {


    public static void main (String [] args) {

        int x = 10;
        int y = 7;
        int z;


        System.out.println("Valorile de dinaintea interschimbarii sunt: " + " x are valoarea: " + x + " y are valoarea: " + y);

//reassignare

        z = y;
        y = x;
        x = z;

        System.out.println("Valorile dupa interschimbare sunt: " + " x are valoarea: " + x + " y are valoarea: " + y + " z are valoarea: " + z);


    }
}
