package java_week1;

public class HelloWorld {

    public static void main(String[] args) {

           /*

            comentariu pentru un rand - programul va sari acel rand, nu il va executa
            se marcheaza inceputul unei metoda - o secventialitate de pasi
            inceput de metoda

            */

        System.out.println("Hello World!");

        //acest rand printeaza un mesaj pe o linie independenta
        //fiecare final de "comanda" este marcat de ;

        System.out.println("Hello World2!");

        System.out.print("Hello World3!");

        System.out.print("Hello World4!");

        //consola este vizibila doar persoanei care ruleaza codul
    }

}