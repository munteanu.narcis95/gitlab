package java_week1.methods;

public class MethodTypes {


    public static void main (String [] args) {

        //metode de tip VOID - afiseaza un rezultat in consola (local), returneaza o valoare nula

        int a = 30;
        int b = 25;

        int suma = a + b;

        System.out.println(suma);


    }

    //metode NON-VOID - returneaza o valoare compusa (logica de calcul) spre a fi folosita/refolosita in proiect, nu afiseaza nimic in consola

    public static int suma(int a, int b) {

        int suma = a+b;

        return suma;


    }


}
