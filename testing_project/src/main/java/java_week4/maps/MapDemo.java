package java_week4.maps;

import java_week4.Cat;
import java_week4.Dog;

import java.util.*;

public class MapDemo {

    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>();

        map.put(1, "Alex");
        map.put(2, "Bogdan");
        map.put(3, "Alex");
        map.put(4, "Catalin");

        map.put(2, "Ciprian");

        System.out.println(map);
        System.out.println(map.size());
//        map.put(5, null);
        System.out.println(map.get(2));

        Map<String, String> map2 = new HashMap<>();
        map2.put("key1", "value1");
        map2.put("key2", "value2");
        map2.put("key3", "value3");

        System.out.println(map2);
        map2.put("key2", "value4");
        System.out.println(map2);

        for (Map.Entry element : map.entrySet()) {
            System.out.println(element.getKey() + " = " + element.getValue());
        }

        map2.remove("key3");
        map2.remove("key2", "value2");
        System.out.println(map2);

        Map<Integer, String> mapNesortat = new LinkedHashMap<>();
        mapNesortat.put(102, "Alex");
        mapNesortat.put(101, "Tudor");
        mapNesortat.put(104, "Bogdan");
        mapNesortat.put(103, "Alex");

        System.out.println(mapNesortat);

        Map<Integer, String> mapSortat = new TreeMap<>(mapNesortat);
        System.out.println(mapSortat);

        Map<Dog, Cat> map3 = new HashMap<>();

        Map<Integer, Map<Integer, String>> map4 = new HashMap<>();

        map4.put(1, mapSortat);
        System.out.println(map4);

        List<Map<Object, Object>> tabel;

    }
}
