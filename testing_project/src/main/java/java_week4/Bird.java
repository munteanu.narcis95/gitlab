package java_week4;

public class Bird extends Animal{

    String species;

    public Bird(String name, int age, String species) {
        super(name, age);
        this.species = species;
    }

    @Override
    public void makeSound(){
        System.out.println("Chirp, Chirp");
    }
}
