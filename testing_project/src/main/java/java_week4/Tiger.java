package java_week4;

public class Tiger extends WildAnimal{

    public Tiger(String name, int age) {
        super(name, age);
    }

    @Override
    public void roam() {
        System.out.println(name +"is roaming in the jungle");
    }
}
