package java_week4.exercise;

public class Product {
    private  int productId;
    private String productName;
    private double price;

    public Product(int productId, String productName, double price) {
        this.productId = productId;
        this.productName = productName;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product ID: " +productId
                +"\nProductName: " +productName
                +"\nPrice: " +price;
    }
}
