package java_week4.exercise;

public interface ProductCatalog {
    void addToCatalog(Product product);

    void displayCatalog();
}
