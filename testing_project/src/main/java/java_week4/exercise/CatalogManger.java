package java_week4.exercise;

public class CatalogManger implements ProductCatalog{
    private Product[] productCatalog;
    private int size;

    public CatalogManger(int size) {
        this.productCatalog = new Product[size];
        this.size = 0; //the catalog is empty
    }

    @Override
    public void addToCatalog(Product product) {
        if(size < productCatalog.length) {
            productCatalog[size] = product;
            size++;
        } else {
            System.out.println("catalog is full");
        }
    }

    @Override
    public void displayCatalog() {
        System.out.println("Catalog de produse:");
        for(int i=0; i < size; i++) {
            System.out.println(productCatalog[i]);
            System.out.println("-------------");
        }
    }
}
