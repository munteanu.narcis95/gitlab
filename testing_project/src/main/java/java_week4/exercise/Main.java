package java_week4.exercise;

public class Main {
    public static void main(String[] args) {
        //Create an instance of catalogmanager
        CatalogManger catalogManger = new CatalogManger(4);

        //Create a product
        Product laptop = new Product(1, "laptop", 99);
        Product laptop2 = new Product(1, "laptop2", 99);

        catalogManger.addToCatalog(laptop);
        catalogManger.addToCatalog(laptop2);
        catalogManger.displayCatalog();
    }

}
