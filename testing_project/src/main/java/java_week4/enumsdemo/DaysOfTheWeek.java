package java_week4.enumsdemo;

public enum DaysOfTheWeek {
    MONDAY("Monday","Luni", 10),
    TUESDAY("Tuesday", "Marti", 20),
    WEDNESDAY("Wednesday","Miercuri", 30),
    THURSDAY("Thursday", 40),
    FRIDAY("Friday", 50),
    SATURDAY("Saturday", 90),
    SUNDAY("Sunday", "Sunday");

    final String day;
    String dayInRomanian;
    final int levelOfEnergy;

    DaysOfTheWeek(String day, String dayInRomanian,int levelOfEnergy) {
        this.day = day;
        this.dayInRomanian = dayInRomanian;
        this.levelOfEnergy = levelOfEnergy;
    }

    DaysOfTheWeek(String day, String dayInRomanian) {
        this.day = day;
        this.dayInRomanian = dayInRomanian;
        levelOfEnergy = 0;
    }

    DaysOfTheWeek(String day, int levelOfEnergy) {
        this.day = day;
        this.levelOfEnergy = levelOfEnergy;
    }
}
