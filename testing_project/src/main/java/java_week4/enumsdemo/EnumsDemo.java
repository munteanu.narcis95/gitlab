package java_week4.enumsdemo;

public class EnumsDemo {

    public static void main(String[] args) {

        String today = "Monday";

        System.out.println(today);
        System.out.println("Trece o zi");

        today = "Tuesday";
        System.out.println(today);

        System.out.println("Trece o zie");
        today = "Wendsday? ++";
        System.out.println(today);

        DaysOfTheWeek realToday = DaysOfTheWeek.MONDAY;

        System.out.println(realToday);
        System.out.println("Trece o ziff");

        realToday = DaysOfTheWeek.TUESDAY;
        System.out.println(realToday);

        System.out.println();

        for(DaysOfTheWeek day : DaysOfTheWeek.values()) {
            System.out.println(day);
        }

        System.out.println();
        System.out.println(realToday.day);
        System.out.println(DaysOfTheWeek.FRIDAY.day);
        System.out.println(DaysOfTheWeek.MONDAY.levelOfEnergy);

        System.out.println("Astazi " + realToday.dayInRomanian + " am " + realToday.levelOfEnergy + "% energie pentru Java");

        System.out.println(DaysOfTheWeek.MONDAY.day);

    }
}
