package java_week4;

public class Animal {
    String name;
    int age;
    final String soundType = "Generic"; //Final field to represent the type of the sound
    public static final double PI = 3.1415;

    private static int totalDogs = 0;

    //Constructor to initilize name and age
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        totalDogs++;
    }

    public void eat(String food){
        System.out.println(name +" is eating " +food);
    }
    //Method to simulate eating
    public void eat(String food, int quantity) {
        System.out.println(name +" is eating " +food +"cantitate " +quantity);
    }

    //Method to simulate a sound for an animal
    public void makeSound() {
        System.out.println(soundType +"animal sound");
    }

    //Static method to obtain the total number of dogs
    public static int getTotalDogs() {
        return totalDogs;
    }
}
