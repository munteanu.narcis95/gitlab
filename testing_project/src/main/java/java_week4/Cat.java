package java_week4;

public class Cat extends Animal implements Pet{
    boolean isStray;

    public Cat(String name, int age, boolean isStray) {
        super(name, age);
        this.isStray = isStray;
    }

    @Override
    public void makeSound(){
        System.out.println("Meow meow");
    }

    @Override
    public void play() {
        System.out.println(name + " jumps around");
    }
}
