package java_week4;

public class Dog extends Animal implements Pet{
    String breed;

    public Dog(String name, int age, String breed) {
        super(name, age);
        this.breed = breed;
    }

    //Overriding the makesound method to provide a specific sound for Dog
    //'Overide' annotation indicated that this method is intended to override a method in the superclass
    @Override
    public void makeSound() {
        System.out.println("Woof wood -a" +soundType +" dog");
    }

    @Override
    public void play() {
        System.out.println(name +" is running with the ball");
    }

    //Override toString to provide a meaniningful representation for dog
    @Override
    public String toString() {
        return "Dog: " +name;
    }

}
