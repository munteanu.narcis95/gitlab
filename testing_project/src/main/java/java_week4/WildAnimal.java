package java_week4;

public abstract class WildAnimal extends Animal{

    public WildAnimal(String name, int age) {
        super(name, age);
    }

    public abstract void roam();
}
