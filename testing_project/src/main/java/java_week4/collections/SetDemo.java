package java_week4.collections;

import java.util.*;

public class SetDemo {

    public static void main(String[] args) {

        Set<Integer> integers = new HashSet<>();

        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(3);

        System.out.println(integers);
        System.out.println(integers.size());

        for (int number : integers) {
            System.out.println(number);
        }

        Set<String> nume = new HashSet<>(List.of("e1", "b4", "c3", "b4"));

        System.out.println(nume);

        Set<String> nume2 = new HashSet<>();
        nume2.add("e1");
        nume2.add("b4");
        nume2.add("c3");
        System.out.println(nume2);
//        nume2.remove("b4");

        Set<String> nume3 = new LinkedHashSet<>(List.of("e1", "b4", "c3", "b4"));

        System.out.println(nume3);

        Set<String> nume4 = new TreeSet<>(nume3);
        System.out.println(nume4);

    }
}
