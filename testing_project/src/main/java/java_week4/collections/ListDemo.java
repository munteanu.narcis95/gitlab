package java_week4.collections;

import java_week4.Dog;

import java.util.*;

public class ListDemo {
    public static void main(String[] args) {

        List<String> names = new ArrayList<>();

        names.add("John");
        System.out.println(names);
        System.out.println(names.size());
        names.add("Marry");

        System.out.println(names);
        System.out.println(names.size());

        names.remove("Marry");
        System.out.println(names);
        System.out.println(names.size());

        names.add("John");
        System.out.println(names);
        System.out.println(names.size());

        names.add("Mary");
        names.add("Anna");
        names.add("Alex");
        System.out.println(names);

        names.remove(3);
        System.out.println(names);

        names.add(4, "Vali");
        System.out.println(names);

        System.out.println(names.get(4) + " este tare!");

        names.remove("John");
        System.out.println(names);

        for ( String name : names) {
            System.out.println(name);
        }

        List<String> elemente = new ArrayList<>(Arrays.asList("el2", "el1", "el3"));
//        List<String> elemente = new ArrayList<>(List.of("el2", "el1", "el3"));

        System.out.println(elemente);
        System.out.println(elemente.size());
        elemente.add("el4");
        System.out.println(elemente);
        System.out.println(elemente.size());

        List<Dog> caini = new ArrayList<>();

        Dog dog = new Dog("Azorel", 20, "bison");
        caini.add(dog);
        caini.add(new Dog("Rex", 12, "ciobanesc"));

        System.out.println(caini);

//        List<List<String>> ...

        System.out.println(elemente);

        Collections.sort(elemente);
        System.out.println(elemente);

        List<String> denumiri = new ArrayList<>(Arrays.asList("A498877", "b9", "aZ", "za", "az"));
        System.out.println(denumiri);

        Collections.sort(denumiri);
        System.out.println(denumiri);

        List<String> elemente2 = new ArrayList<>(Arrays.asList("el2", "el1", "el3", "el3", "el2"));
        System.out.println(elemente2);

        Set<String> elemente2Neduplicate = new HashSet<>(elemente2);
        System.out.println(elemente2Neduplicate);


    }
}
