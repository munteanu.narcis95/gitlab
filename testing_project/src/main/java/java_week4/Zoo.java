package java_week4;

public class Zoo {

    public static void main(String[] args) {
        Animal dog = new Dog("Buddy", 3, "Labrador");
        Animal cat = new Cat("whiskers", 2, true);
        Animal bird = new Bird("Stripes", 5, "Parrot");
        WildAnimal tiger = new Tiger("stripes", 5);


/*        animalInfo(dog);
        animalInfo(cat);
        animalInfo(bird);
        animalInfo(tiger);*/

        //Using pet interface
        Pet petDog = new Dog("Max", 4, "Poodle");
        Pet petCat = new Cat("Mikes", 3, true);
        petDog.play();
        petCat.play();

        System.out.println("Total number of animalsin zoo: " +Animal.getTotalDogs());

        System.out.println(dog.toString());


        //Accesing static members from the Animal class
    }


    public static void animalInfo(Animal animal) {
        System.out.println("Details of " +animal.name +":");
        if(animal instanceof Dog) {
            //if the animal is an instance of God, execute the following block
            animal.eat("meat");
        } else if (animal instanceof Cat) {
            animal.eat("fish");
        } else if(animal instanceof Bird) {
            animal.eat("worms");
        } else if(animal instanceof Tiger) {
            animal.eat("zebra");
            //Cast the 'animal' reference to WildAnimal
            ((Tiger) animal).roam();
        }
        animal.makeSound();
    }

    //


}