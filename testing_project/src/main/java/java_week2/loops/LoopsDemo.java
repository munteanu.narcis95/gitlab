package java_week2.loops;

public class LoopsDemo {

    public static void main(String[] args) {

        // while
        int i = 1;
        while (i <= 10) {
            if(i!=5) {
                System.out.println("While te saluta #" + i);
            }
            i++;
//            i+=2; // i = i+2;
        }
        System.out.println("=== Final while ===");
        System.out.println();

        i = 1;
        boolean esteCaldAfara = true;
        if (esteCaldAfara) {
            while (i <= 10) {
                if(i!=5) {
                    System.out.println("While e cald afara #" + i);
                }
                i++;
            }
            System.out.println("=== Final while e cald afara ===");
        } else {
            while (i <= 10) {
                System.out.println("While e cald afara #" + i);
            i+=2; // i = i+2;
            }
            System.out.println("=== Final while e cald afara ===");
        }

        salutDeNOri(1);
        salutDeNOri(5);

        // do-while
        i = 1;
        do {
            System.out.println("Do While te saluta #" + i);
            i++;
        } while (i <= 10);
        System.out.println("=== Final do-while ===");
        System.out.println();

        salutDeLaDoWhileDeNOri(3);

        // for
        for (int j = 7; j <= 7; j++) {
            System.out.println("For te saluta #" + j);
        }
        System.out.println("=== Final do-while ===");

        salutDeLaForDeNOri(3);
        salutDeLaForDeNOri(7, 13);
    }

    public static void salutDeNOri(int n) {
        int i = 1;
        while (i <= n) {
            System.out.println("Metoda While te saluta #" + i);
            i++;
        }
        System.out.println("=== Final metoda while ===");
    }

    public static void salutDeLaDoWhileDeNOri(int n) {
        int i = 1;
        do {
            System.out.println("Metoda Do-While te saluta #" + i);
            i++;
        } while (i <= n);
        System.out.println("=== Final metoda do-while ===");
    }

    public static void salutDeLaForDeNOri(int n) {
        for (int i = 1; i<=n; i++) {
            System.out.println("Metoda For te saluta #" + i);
        }
        System.out.println("=== Final metoda for ===");
    }

    public static void salutDeLaForDeNOri(int start, int finish) {
        for (int i = start; i<=finish; i++) {
            System.out.println("S " + i);
//            break;
            // vs
//            return;
        }
        System.out.println("=== Finish ===");
    }
}
