package java_week2.loops;

public class BreakAndContinueDemo {

    public static void main(String[] args) {


        for (int i = 10; i<= 50; i+=10) {
            if (i == 30) {
                System.out.println("posibil "+ i);
                break;
//                System.out.println("ceva");
            }

            if (i == 20) {
                break;
            }
            System.out.println(i);
        }

        System.out.println("End for");

        int j = 10;
        while (j <= 50) {
            if (j == 30) {
                break;
            }
            System.out.println(j);
            j+=10;
        }

        System.out.println("End while");

        for (int i = 10; i<= 50; i+=10) {
            if (i == 30) {
//                System.out.println("posibil " + i);
                continue;
            }
            System.out.println(i);
        }

        System.out.println("End for with continue");

//        int c1 =1;
//        int c2 =2;
//        int c3 =3;
//
//        while (c1 !=0 && c2 != 0 && c3!=0) {
//            System.out.println("CEva");
//            c1 = 0;
//        }
    }
}
