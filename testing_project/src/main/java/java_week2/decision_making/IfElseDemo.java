package java_week2.decision_making;

public class IfElseDemo {

    public static void main(String[] args) {

        int nr1 = 20;
        int nr2 = 30;

        if (nr1 > nr2) {
            System.out.println("Numarul cel mai mare este: " + nr1);
        } else {
            System.out.println("Numarul cel mai mare este: " + nr2);
        }

        int nr3 = 45;
        int nr4 = 80;

//        if (nr3 > nr4) {
//            System.out.println("Numarul cel mai mare este: " + nr1);
//        } else {
//            System.out.println("Numarul cel mai mare este: " + nr4);
//        }
        int rezultat = comparareaADouaNumere(nr3, nr4);
        System.out.println("Rzultatul este: " + rezultat);

        System.out.println("Rezultatul direct este: " + comparareaADouaNumere(nr3, nr4));
        System.out.println("Rezultatul direct fara variabile este: " + IfElseDemo.comparareaADouaNumere(67, 90));

        System.out.println("Rezultatul folosind metoda din exterior este: " + MetodeDeComparare.comparareaADouaNumere(58,89));

        afisareComparareaADouaNumere(46, 45);

        afisareComparareaADouaNumere(rezultat, 67);

        afisareComparareaADouaNumere(1094324, 109435);

        if (nr1 > nr2) {
            System.out.println("Numarul 1 este cel mai mare cu valoarea: " + nr1);
        } else if (nr1 == nr2) { // nr1 <= nr2
            System.out.println("Numerele sunt egale cu valoarea: " + nr2);
        } else { //if (nr1 < nr2)
            System.out.println("Numarul 2 este cel mai mare cu valoarea: " + nr2);
        }

        if (2 > 3)
            System.out.println("Gresit");
        System.out.println("Adevarat");


        char  grade = 'X';

        if (grade == 'A') {
            System.out.println("Excellent");
        } else if (grade == 'B' || grade == 'C'){
            System.out.println("Well done!");
        } else if (grade == 'D') {
            System.out.println("You passed.");
        } else if (grade == 'F') {
            System.out.println("Try again!");
        } else if (grade == 'X') {
            System.out.println("Go home");
        } else {
            System.out.println("Invalid grade");
        }

    }

    public static int comparareaADouaNumere(int x, int y) {
        if (x > y) {
            return x;
        } else {
            return y;
        }
    }

    public static void afisareComparareaADouaNumere( int x, int y) {
        if (x > y) {
            System.out.println("Numarul mai mare este " + x );
        } else {
            System.out.println("Numarul mai mare este " + y );
        }
    }
}
