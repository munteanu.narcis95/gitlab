package java_week2.decision_making;

public class SwitchDemo {

    public static void main(String[] args) {

        int nota = 23;
//        String calificativ;
        afisareCalificativDinNota(nota);

        afisareCalificativDinNota(2);
        afisareCalificativDinNota(9);

        int nota2 = 0;
        afisareCalificativDinNota(nota2);

        String calificativ = calificativDinNota(10);
        System.out.println("Returneaza: " + calificativ);
    }

    public static void afisareCalificativDinNota(int nota) {
        switch (nota) { // nota == case
            case 10:
                System.out.println("Excellent");
//                calificativ = "Excellent";
                break;
            case 9:
            case 8:
            case 7:
                System.out.println("Well done");
                break;
            case 6:
            case 5:
                System.out.println("You passed");
                break;
            case 4:
            case 3:
            case 2:
                System.out.println("Hai ma chiar 2");
//                System.out.println("Try again");
//                break;
            case 1:
                System.out.println("Try again");
                break;
            default:
                System.out.println("Invalid grade");
        }
    }

    public static String calificativDinNota(int nota) {
        switch (nota) {
            case 10:
//                System.out.println("Excellent");
                return "Excellent";
//                break;
            case 9:
            case 8:
            case 7:
                return "Well done";
            case 6:
            case 5:
                return "You passed";
            case 4:
            case 3:
            case 2:
            case 1:
                return "Try again";
            default:
                return "Invalid grade";
        }
    }
}
