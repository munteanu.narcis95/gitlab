package java_week2.arrays;

import java.util.Arrays;

public class AraysDemo {

    public static void main(String[] args) {


        int[] numereIntregi;
//        System.out.println(Arrays.toString(numereIntregi));
        String[] numeDePersoane = new String[5];

        numereIntregi = new int[]{1, 2, 3, 4};
        System.out.println(Arrays.toString(numereIntregi));
//        numereIntregi[0] = "ceva";

        System.out.println(numereIntregi[0]);

        int[] alteNumereIntregi = {/* 0 */ 39, /* 1 */ 10, /* 2 */ 51, /* 3 */2}; // L = 4 ; I = L-1
        System.out.println(alteNumereIntregi[2]);
        System.out.println("Lungimea sirul " + alteNumereIntregi.length);
        System.out.println("Lungimea sirul de nume " + numeDePersoane.length);

        System.out.println(Arrays.toString(numeDePersoane));
        numeDePersoane[4] = "Andrei";
        System.out.println(Arrays.toString(numeDePersoane));
        numeDePersoane[2] = numeDePersoane[4] = "Cristi";

        System.out.println(Arrays.toString(numeDePersoane));
        System.out.println();

        System.out.print("{");
        for (int i = 0; i<= numereIntregi.length - 1 ; i++) {
            System.out.println("Index " + i + " are valoarea: " + numereIntregi[i]);
        }
        System.out.println("}");

//        System.out.println(numereIntregi);

        Arrays.sort(alteNumereIntregi);
        for (int e : alteNumereIntregi) {
            System.out.println("Indexul !!!!" + " Valoare " + e);
        }

    }
}
