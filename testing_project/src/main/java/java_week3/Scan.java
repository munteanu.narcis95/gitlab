package java_week3;
import java.util.Scanner;
public class Scan {

    public static void main (String [] args) {

        //pasul 1 - declaram variabilele cu care vrem sa lucram si le dam o valoare de inceput/baza precum 0


        long a, b, c, d, suma = 0, media=0, calcul = 0, calculsecundar = 0;


        //pasul 2 - apelam libraria Scanner care ii spune programului sa ne solicite introducerea de date de la tastatura

        Scanner scan = new Scanner(System.in);

        //pasul 3 - implementarea propriu-zisa in care dam valori de la tastatura

        a = scan.nextInt();

        b = scan.nextInt();

        c = scan.nextInt();

        d = scan.nextLong();

        // pasul 4 - defini un comportament pentru variabilele carora le-am dat valori

        suma = a+b+c;

        media = suma/4;

        calcul = suma % d;

        calculsecundar = calcul / a;

        System.out.println(suma);
        System.out.println(media);



    }




}
