package java_week3.oop.inheritance;

public class Vehicle {

    double speed;

    String type;

    //definesc un behaviour general

    public void go () {

        System.out.println("This vehicle is moving");
    }

    public void stop () {

        System.out.println("This vehicle stopped");
    }

}
