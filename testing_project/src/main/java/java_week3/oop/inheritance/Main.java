package java_week3.oop.inheritance;

public class Main {

    public static void main (String [] args) {

        Car myCar = new Car();
        myCar.go();
        Bicycle myBike = new Bicycle();
        myBike.stop();

        System.out.println(myCar.colour);
        System.out.println(myBike.colour);
        System.out.println(myCar.doors);


    }
}
