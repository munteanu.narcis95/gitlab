package java_week3.oop.abstraction;

public abstract class Animal {

    int age;

    String name;

    //fac metoda abstracta pentru a limita accesul la detaliile ei
    //daca discutam de o metoda abstracta, automat trebuie noi sa facem o implementare ai ei in celelalte clase si respectiv sa nu specificam corpul metodei
    public abstract void makeASound();


}
