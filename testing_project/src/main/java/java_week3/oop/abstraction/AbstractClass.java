package java_week3.oop.abstraction;

//prin principiul encapsularii se ascund informatii despre anumite varibile, prin abstractizare putem spune ca limitam accesul la ce anume este definit intr-o clasa abstracta
//o clasa abstracta nu imi permite sa instantiez - nu pot crea obiecte din ea
public class AbstractClass {

    public void main (String [] args) {

        Cat kitty = new Cat(); //folosesc un constructor method pentru a crea un obiect de tipul clasei Cat

    }



}
