package java_week3.oop.anotherExample;

public class PersonDemo {

    public static void main(String[] args) {
        // doar din clasa pot crea un obiect folosind constructor

        Person person1 = new Person();
        person1.age = 20;
        person1.city = "Iasi";
        person1.name = "Alexandra";

        Person person2 = new Person();

        person2.age=30;
        person2.city = "Bucuresti";
        person2.name = "Maria";

        SoftSkills communicationLevel1 = new SoftSkills();

        communicationLevel1.englishLevel="B2";
        communicationLevel1.romanianLevel = "C2";


        //persoana 1 primeste communicationLevel 1

        person1.communicationStatus = communicationLevel1;


    }
}
