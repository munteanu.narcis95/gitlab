package java_week3.oop.polymorphism;

public class Dog extends Animal {
    //clasa dog este clasa copil a clasei Animal deoarece preia proprietatile prin extends
    //clasa Animal devine clasa parinte sau clasa super

    String breed;
    int age;

    public String getBreed() {
        return breed;

    }

    public void setBreed (String breed) {

        this.breed = breed;

    }
    //override = supra-scriere, dau un alt comportament metodei definite initial in clasa Animal (in contextul variabilelor override are loc in re-assignam o valoare)
    //pentru a discuta de polymorphism, trebuie deci sa facem override la metoda deja definita, care capata o alta forma
    //metoda la care facem override trebuie sa fie din clasa parinte, trebuie sa arate diferit si ideal sa se comporte diferit dpdvd parametrii sau return type

    @Override
    public void eat() {
        System.out.println("meat");
    }
}
