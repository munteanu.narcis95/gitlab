package java_week3;

public class ThisConflictExample {
    //variabile de instanta
    // private inseamna ca pot accesa doar in interiorul clasei ceea ce este definit
    private int x; //variabila de instanta x

    public ThisConflictExample(int x) {
        this.x = x;

    }

    public void setValue(int x ) {

        this.x = x; //conflict intre paramtrul x primit de metoda setValue si respectiv variabila x
        //x va primi valoarea parametrului metodei in locul variabilei


    }

    public void displayValue (){
        System.out.println(x); //printeaza valoarea lui x

    }

    public static void main (String[] args) {

        ThisConflictExample myExample = new ThisConflictExample(12);

        myExample.setValue(17);

        myExample.displayValue();

        //va afisa 12 pentru exista un conflict intre parametru si variabila de instanta, avand acelasi nume
        //pentru ca java sa stie ca noi vrem sa setam valoarea variabilei de instanta cu cea pe care o primeste prin intermediul parametrului, vom folosi 'this'

    }

}
