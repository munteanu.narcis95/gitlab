package java_week3;
import java.util.Arrays;

public class ArraySort {

    public static void main (String [] args) {

        int [] numbers = {3, 5, 20, 9, 1, 13, 87, 22, 31, 4};
        //tipul de data .(metoda = "functia predefinita")

        Arrays.sort(numbers); //s-a sortat (ordonat crescator) array-ul dat

        //metoda = unul sau o serie de comportamente pe care trebuie sa le preiau pentru array-ul meu

        showNumbers(numbers);


    }
    // pentru a scoate elementele din array avem nevoie sa il parcurgem ("interogam"/"apelam") si sa extragem valorile din el rand pe rand

    // definesc un comportament = metoda  - prelucrez niste date - elementele din array
    public static void showNumbers (int[] numbers) {

        for (int i=0; i < numbers.length; i++) {

            System.out.println(numbers[i] + " ");

        }

        String numbersString = Arrays.toString(numbers);

        System.out.println(numbersString);

      //sorteaza elementele dintr-un array, transforma-le in sir de caractere si afiseaza

    }

}
