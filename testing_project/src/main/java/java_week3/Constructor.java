package java_week3;

public class Constructor {

    public static void main (String[] args) {

        SecondCar myCar = new SecondCar("Audi", 2000); // - Constructor method = o metoda care apeleaza clasa
        //java va sti ca noi exista posibilitatea sa realizam niste obiecte pe baza acelei clase si in mod automat va crea in spate un constructor method default
        //fiecare obiect de tipul clasei SecondCar va avea particularitatile din Second Car

        System.out.println(myCar.anFabricatie + " " + myCar.marca);

        //un constructor default nu are parametri si implicit nu contine si nu atribuie valori
    }



}
