package java_week3;
// scopul este de a crea un obiect (are proprietati si comportament) de tip masina si sa pot sa ma folosesc de acest obiect pentru a grea altele cu diferite particularitati
public class Car {

        // FIELD-URI/ATRIBUTE ( = proprietati, caracteristici)

        String marca;

        String culoare;

        String model;

        CarEngine motor; //motor = un obiect de tipul "CarEngine"

        CarWheels[] roti; //roti = un obiect de tipul "Car Wheels"

        // BEHAVIOUR - metode
        // definim o metoda de start a masinii
        // daca puterea motorului este mai mica decat 100, masina face "Vrum" =va afisa
        // daca puterea motorului este mai mare decat 100, masina face " Vruuum, vruuum" = va afisa

        public void start() {
            if (motor.putere < 100) {
                System.out.println("Vruuum");
            } else {
                System.out.println("Vruuum, vrruum");
            }

        }

        // definim o metoda/comportament general care printeaza specificatiile masinilor

    public String CarDescription() {

            return "Brand: " + marca + " " + "Culoare: " + culoare + " " + "Model: " + model;

    }




    }





