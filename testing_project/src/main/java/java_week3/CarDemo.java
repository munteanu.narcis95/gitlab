package java_week3;

public class CarDemo {

    public static void main (String[] args)
    {
        //Car este clasa - definim o noua instanta a clasei car care preia particularitatile acesteia

        /* car1 = care vrem sa fie un obiect de tipul car => constructor = este o metoda speciala a unei clase care
          este apelata atunci cand un obiect este creat din acea clasa;
          constructor se foloseste atunci cand dorim sa dam valori field-urilor din clasa apelata
          cand definim un constructor se rezerva un spatiu de memorie nou alocat acestuia
         */
        Car car1 = new Car();
        car1.model = "Q5";
        car1.culoare ="Alb";
        car1.marca = "Audi";


        CarEngine motor1 = new CarEngine();
        motor1.putere = 300;
        motor1.capacitateCilindrica = 2500;

      // = "primeste o valoare..."
        // motorul masinii car 1 primeste engine 1 (toate particularitatile engine 1)

        car1.motor = motor1;


        car1.roti = new CarWheels[] {new CarWheels(), new CarWheels(), new CarWheels(), new CarWheels()};

        Car car2 = new Car();
        car2.marca = "Mercedes";
        car2.model = "GLA";
        car2.culoare = "Verde";

        CarEngine motor2 = new CarEngine();
        motor1.putere = 300;
        motor1.capacitateCilindrica = 2500;

        Car car3 = new Car();
        car3.model = "GLA";
        car3.culoare = "Rosu";

//
        System.out.println();

        //descrierea masinii 1

        System.out.println("Masina 1: " + car1.CarDescription());


    }




}
